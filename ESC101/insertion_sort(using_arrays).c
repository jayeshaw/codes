#include<stdio.h>
#define scani(a) scanf("%d",&a)
#define printi(a) printf("%d",a)

void isort(int [],int,int);
void disp(int [],int);
void insert(int [],int ,int);

int main()
{
    //  Insert your code here.
    int n,num;
    scani(n);
    int arr[n];
    for(int i=0;i<n;i++){
        scani(arr[i]);
    }
    for(int i=0;i<n;i++){
        isort(arr,i,n);
        disp(arr,n);
    }
    return 0;
}

void isort(int arr[],int index,int n){
    for(int i=0;i<index;i++){
        if(arr[index]<arr[i]){
            insert(arr,index,i);
            break;
        }
    }
    return;
}

void insert(int arr[],int index,int dst){
    int temp=arr[index];
    for(int i=index;i>dst;i--){
        arr[i]=arr[i-1];
    }
    arr[dst]=temp;
}

void disp(int arr[],int n){
    for(int i=0;i<n;i++){
        printf("%d",arr[i]);
        if(i!=n-1)  printf(" ");
    }
    printf("\n");
}