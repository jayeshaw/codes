#include <stdio.h>
#define N 10
void multiply(int mat1[][N],int mat2[][N],int mult[][N])
{
    //Fill this area with your code
    int sum=0;
    for(int i=0;i<10;i++){
        for(int j=0;j<10;j++){
            sum=0;
            for(int k=0;k<10;k++)   sum+=mat1[i][k]*mat2[k][j];
            mult[i][j]=sum;
        }
    }
}
int main() {
	// Fill this area with your code.
	int mat1[10][10];
	int n,m;
	scanf("%d %d",&n,&m);
	for(int i=0;i<10;i++){
	    for(int j=0;j<10;j++){
	        if(i<n&&j<m)
	            scanf("%d",&mat1[i][j]);
	        else    mat1[i][j]=0;
	    }
	}
	int n1, m1;
	int mat2[10][10];
	scanf("%d %d",&n1,&m1);
	for(int i=0;i<10;i++){
	    for(int j=0;j<10;j++){
	        if(i<n1&&j<m1)
	            scanf("%d",&mat2[i][j]);
	        else    mat2[i][j]=0;
	    }
	}
	int mult[10][10];
	multiply(mat1,mat2,mult);
	for(int i=0;i<n;i++){
	    for(int j=0;j<m1;j++){
	        printf("%d ",mult[i][j]);
	    }
	    printf("\n");
	}
	return 0;
}