#include<stdio.h>
#include<stdlib.h>

void count_around(int **src,int **A,int i,int j,int n){
    for(int a=i-1;a<=i+1;a++){
        for(int b=j-1;b<=j+1;b++){
            if(a==i&&b==j)  continue;
            if(a<0||a>n-1)  continue;
            if(b<0||b>n-1)  continue;
            else if(src[a][b]==1)    A[i][j]++;
        }
    }
}
int **gen_hint(int **A, int n) {
    int **res;
    res=(int**)malloc(n*sizeof(int*));
    for(int i=0;i<n;i++)    res[i]=(int*)malloc(n*sizeof(int));
    for(int i=0;i<n;i++){
        for(int j=0;j<n;j++){
            count_around(A,res,i,j,n);
        }
    }
    for(int i=0;i<n;i++){
        for(int j=0;j<n;j++){
            if(A[i][j]==1)    res[i][j]=-1;
        }
    }
    return res;
}

int main(){
    int n;
    scanf("%d",&n);
    int **arr;
    arr=(int**)malloc(n*sizeof(int*));
    for(int i=0;i<n;i++)    arr[i]=(int*)malloc(n*sizeof(int));
    char in;
    for(int i=0;i<n;i++){
        for(int j=0;j<n;j++){
            if(j==0)    scanf("%c",&in);
            scanf("%c",&in);
            if(in=='x') arr[i][j]++;
        }
    }
    
    int **res=gen_hint(arr,n);
    for(int i=0;i<n;i++){
        for(int j=0;j<n;j++){
            printf("%d ",res[i][j]);
        }
        printf("\n");
    }
    
    return 0;
}