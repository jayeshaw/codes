#include<stdio.h>

long long  int form(long long int num){
    return num*(num+1)/2;
}

long long int binary(long long int n, long long int lb,long long int ub){
    long long int med=(lb+ub)/2;
    if(form(med)==n)    return med;
    else if(form(med)<n){
        if(form(med+1)>n)   return med;
        else return binary(n, med, ub);
    }
    else{
        if(form(med-1)<n)   return (med-1);
        else return binary(n, lb, med);
    }
    
}
int main(){
    int cases;
    long long  int n,ans;
    long long int ub=1e9;
    scanf("%d",&cases);
    while(cases--){
        scanf("%ld",&n);
        ans=binary(n,0,ub);
        printf("%ld\n",ans);
    }
    return 0;
}