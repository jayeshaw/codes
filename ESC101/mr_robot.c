#include<stdio.h>
#include<stdlib.h>
struct node{
    //define the details of the structure
    int data;
    struct node *next;
};
typedef struct node NODE;
void printList(NODE *);//function to print the solution
NODE* input();//Function to take input
NODE* solve(NODE*);//function to solve the problem
NODE* makeNode(int);
//define any function prototype you want to use


int main()
{
    NODE* head;
    head=input();
    // printList(head);
    printList(solve(head));
    return 0;
}

NODE* input(){
    NODE* hd,*temp;
    int num;
    scanf("%d",&num);
    hd=(NODE*)malloc(sizeof(NODE));
    hd->data=num;
    temp=hd;
    scanf("%d",&num);
    while(num!=-1){
        temp->next=makeNode(num);
        temp=temp->next;
        scanf("%d",&num);
    }
    return hd;
}

NODE* makeNode(int val){
    NODE* nd;
    nd=(NODE*)malloc(sizeof(NODE));
    nd->data=val;
    nd->next=NULL;
    return nd;
}

void printList(NODE* head){
    while(head!=NULL){
        printf("%d",head->data);
        // if(head->next!=NULL)    printf("->");
        head=head->next;
    }
    printf("\n");
}

NODE* solve(NODE* head){
    NODE* copy;
    copy=head;
    int length=0,count=1;
    while(copy!=NULL){
        length++;
        copy=copy->next;
    }
    copy=head;
    while(count<length-head->data){
        copy=copy->next;
        count++;
    }
    if(length>head->data)
        copy->next=copy->next->next;
    head=head->next;
    return head;
}
/*Do not modify the main function
 *Define the structure NODE properly
 *Write the body of the above mentioned function*/
 
//Write the body for all function definitions below