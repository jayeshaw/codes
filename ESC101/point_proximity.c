#include<stdio.h>
#include<stdlib.h>

int euclid(int**,int,int,int);
int main()
{
    //  Insert your code here.
    int n,k,query;
    scanf("%d %d",&n,&k);
    scanf("%d",&query);
    int **arr;
    arr=(int**)malloc(n*sizeof(int*));
    for(int i=0;i<n;i++){
        arr[i]=(int*)malloc(k*sizeof(int));
    }
    char **chr;
    chr=(char**)malloc(n*sizeof(char*));
    for(int i=0;i<n;i++){
        chr[i]=(char*)malloc(1000*sizeof(char));
    }
    for(int i=0;i<n;i++){
        for(int j=0;j<k;j++){
            scanf("%d",&arr[i][j]);
        }
        scanf("%s",chr[i]);
    }
    int distance[n];
    for(int i=0;i<n;i++)    distance[i]=euclid(arr,i,k,query-1);
    int max_index=0;
    for(int i=0;i<n;i++){
        if(distance[i]>distance[max_index]) max_index=i;
    }
    printf("%s\n",chr[max_index]);
    printf("%d",distance[max_index]);
    return 0;
}

int euclid(int **arr,int i,int k,int q){
    int dist=0;
    for(int a=0;a<k;a++){
        dist+=(arr[i][a]-arr[q][a])*(arr[i][a]-arr[q][a]);
    }
    return dist;
}