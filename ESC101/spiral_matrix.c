#include<stdio.h>
#include<stdlib.h>

void construct(int**,int,int,int ,int,int,int);
int main()
{
    //  Insert your code here.
    int n;
    scanf("%d",&n);
    int **arr;
    arr=(int**)malloc(n*sizeof(int*));
    for(int i=0;i<n;i++){
        arr[i]=(int*)calloc(n,sizeof(int));
    }
    
    construct(arr,0,0,n-1,1,1,0);
    for(int i=0;i<n;i++){
        for(int j=0;j<n;j++){
            printf("%d ",arr[i][j]);
        }
        printf("\n");
    }
    return 0;
}

void construct(int **arr,int x,int y,int k,int mode,int dir,int start){
    if(k==-1)   return;
    if(mode==1){
        if(dir==1){
            for(int i=0;i<=k;i++)    arr[y][x+i]=start+i+1;
            construct(arr,x+k,y+1,k-1,2,1,start+k+1);
        }
        else{
            for(int i=0;i<=k;i++)   arr[y][x-i]=start+i+1;
            construct(arr,x-k,y-1,k-1,2,-1,start+k+1);
        }
    }
    else{
        if(dir==1){
            for(int i=0;i<=k;i++)    arr[y+i][x]=start+i+1;
            construct(arr,x-1,y+k,k,1,-1,start+k+1);
        }
        else{
            for(int i=0;i<=k;i++)   arr[y-i][x]=start+i+1;
            construct(arr,x+1,y-k,k,1,1,start+k+1);
        }
    }
}